FROM openjdk:8-alpine

RUN apk add --no-cache  curl grep sed unzip

WORKDIR /root

RUN curl --insecure -o ./sonarscanner.zip -L https://sonarsource.bintray.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-$VERSION-linux.zip

RUN ls -a

RUN unzip /root/sonarscanner.zip
RUN rm /root/sonarscanner.zip

RUN mv /root/sonar-scanner-cli-$VERSION-linux /root/sonar-scanner

ENV SONAR_RUNNER_HOME=/root/sonar-scanner
ENV PATH $PATH:/root/sonar-scanner/bin

WORKDIR /root/sonar-scanner/bin/
